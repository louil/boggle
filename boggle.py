#!/usr/bin/python3

import sys
assert sys.version_info >= (3,0,"Incorrect version of python. Requires 3.0 or higher")

import boggle_class as b
import random
import time
import os
import re
import curses

#http://stackoverflow.com/questions/746082/how-to-find-list-of-possible-words-from-a-letter-matrix-boggle-solver#750012
def solve(grid, words, prefixes, nrows, ncols):
    for y, row in enumerate(grid):
        for x, letter in enumerate(row):
            if letter == 'q':
                letter = 'qu'
            for result in extending(grid, prefixes, letter, ((x, y),), words, nrows, ncols):
                yield result

#http://stackoverflow.com/questions/746082/how-to-find-list-of-possible-words-from-a-letter-matrix-boggle-solver#750012
def extending(grid, prefixes, prefix, path, words, nrows, ncols):
    if prefix in words:
        yield (prefix, path)
    for (nx, ny) in neighbors(path[-1], nrows, ncols):
        if (nx, ny) not in path:
            if grid[ny][nx] == 'q':
                prefix1 = prefix + 'qu'
            else:
                prefix1 = prefix + grid[ny][nx]
            if prefix1 in prefixes:
                for result in extending(grid, prefixes, prefix1, path + ((nx, ny),), words, nrows, ncols):
                    yield result

#http://stackoverflow.com/questions/746082/how-to-find-list-of-possible-words-from-a-letter-matrix-boggle-solver#750012
def neighbors(variable, nrows, ncols):
    (x, y) = variable
    for nx in range(max(0, x-1), min(x+2, ncols)):
        for ny in range(max(0, y-1), min(y+2, nrows)):
            yield (nx, ny)

def randomize_grid(letter):
    temp_list = list(letter)
    order_list = []

    while True:
        x = random.choice(temp_list)
        order_list.append(x)
        temp_list.remove(x)
        if not temp_list:
            break
    return order_list

def show_the_grid(stdscr, letter):
    boxes = curses.newwin(9, 13)
    boxes.box()
    stdscr.refresh()
    boxes.refresh()
    stdscr.move(1, 3)
    stdscr.vline('|', 7)
    stdscr.move(1, 6)
    stdscr.vline('|', 7)
    stdscr.move(1, 9)
    stdscr.vline('|', 7)
    stdscr.move(2, 1)
    stdscr.hline('-', 11)
    stdscr.move(4, 1)
    stdscr.hline('-', 11)
    stdscr.move(6, 1)
    stdscr.hline('-', 11)

    index = 0
    for row in range(1,9,2):
        for column in range(1,13,3):
            if letter[index] == 'Q':
                stdscr.addstr(row,column, 'QU')
            else:
                stdscr.addstr(row, column, letter[index])
            index += 1

def show_the_answer_box(guessed_word, new_window):
    tracker = 1
    column = 1
    (y,x) = new_window.getmaxyx()
    
    for ze_word in guessed_word:
        if tracker >= y-1:
            column+=18
            tracker = 1
        else:
            new_window.addstr(tracker, column, ze_word)
            tracker+=1
    new_window.refresh()

def print_player_answer(ze_word, guessed_word, new_window):
    tracker = 1
    column = 1
    (y,x) = new_window.getmaxyx()
    
    for ze_word in guessed_word:
        if (tracker) > 21:
            new_window.scroll()
            new_window.addstr(21, column, ze_word, curses.color_pair(2))
            new_window.box()
        else:
            new_window.addstr(tracker, column, ze_word, curses.color_pair(2))
            tracker+=1   
    new_window.refresh()

def print_computer_answer(ze_word, computer_word, new_window2):
    tracker = 1
    column = 1
    (y,x) = new_window2.getmaxyx()
    
    for ze_word in computer_word:
        if (tracker) > 21:
            new_window2.scroll()
            new_window2.addstr(21, column, ze_word, curses.color_pair(3))
            new_window2.box()
        else:
            new_window2.addstr(tracker, column, ze_word, curses.color_pair(3))
            tracker+=1   
    new_window2.refresh()

def get_choice():
    cursor_place = 0
    new_choice = ""
    user_answer = ""
    y = 1
    x = 15
    choice = 0
    while True:
        choice = stdscr.getch(y, x+cursor_place)
        stdscr.addstr(5,15,str(choice))
        if chr(choice) == '\n':
            user_answer = new_choice
            break
        else:
            new_choice+=str(chr(choice))
            cursor_place+=1

    return user_answer

def main(stdscr):
    die = b.Board()
    guessed_word = []
    computer_word = []
    ze_word = ""
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)

    letter = [random.choice(i) for i in die._dice.values()]
    random.shuffle(letter)
    counter = 1

    grid = ""
    for l in letter:
        grid += str(l).lower()
        if counter % 4 == 0:
            grid += ' '
        counter += 1
    grid = tuple(grid.split())
    nrows, ncols = len(grid), len(grid[0])

    # A dictionary word that could be a solution must use only the grid's
    # letters and have length >= 3. (With a case-insensitive match.)
    alphabet = ''.join(set(''.join(grid)))
    if 'q' in alphabet:
        alphabet += 'u'
    bogglable = re.compile('[' + alphabet + ']{3,}$', re.I).match

    words = set(word.rstrip('\n') for word in open('/usr/share/dict/words') if bogglable(word))
    prefixes = set(word[:i] for word in words
           for i in range(2, len(word)+1))

    correct_words = ' '.join(sorted(set(word for (word, path) in solve(grid, words, prefixes, nrows, ncols)))).split()
    global all_words
    all_words = ' '.join(sorted(set(word for (word, path) in solve(grid, words, prefixes, nrows, ncols)))).split()

    redraw = True
    stdscr.nodelay(1)
    t_end = time.time() + 60*3
    current_time = start_time = time.time()
    game_time = t_end - start_time + 1
    show_the_grid(stdscr, letter)
    cursor_place = 0
    new_choice = ""
    user_answer = ""
    blank_string= " "
    choice = 0 
    computer_time = time.time() + 7
    global player_score
    global computer_score
    player_score = 0
    computer_score = 0

    (y,x) = stdscr.getmaxyx()
    new_window = curses.newwin(y-1, 18, 1, x-55)
    new_window.box()
    new_window.setscrreg(1, 21)
    new_window.scrollok(True)
    stdscr.refresh()
    new_window.refresh()

    new_window2 = curses.newwin(y-1, 18, 1, x-37)
    new_window2.box()
    new_window2.setscrreg(1, 21)
    new_window2.scrollok(True)
    new_window2.refresh()

    #Format of this timer derived from samuels
    new_window3 = curses.newwin(3, 7, 1, x-65)
    new_window3.box()
    seconds = int((game_time%60))
    minutes = int((game_time/60))
    new_window3.addstr(1, 1, "{0:02}:{1:02}".format(minutes,seconds), curses.color_pair(1))
    new_window3.refresh()
    
    y = 11
    x = 0

    curses.echo() 
    stdscr.addstr(10, 0, "Make a guess: ")
    stdscr.addstr(16, 0, "Player is green", curses.color_pair(2))
    stdscr.addstr(17, 0, "Computer is yellow", curses.color_pair(3))

    while time.time() < t_end:
        stdscr.addstr(14, 0, "Player score is: "+str(player_score))
        stdscr.addstr(15, 0, "Computer score is: "+ str(computer_score))
        if time.time() - current_time > 1:
            game_time -= 1
            seconds = int((game_time%60))
            minutes = int((game_time/60))
            new_window3.addstr(1, 1, "{0:02}:{1:02}".format(minutes,seconds), curses.color_pair(1))
            new_window3.box()
            new_window3.refresh()
            current_time = time.time()

        if computer_time < time.time():
            computer_guess = random.choice(correct_words)
            computer_word.append(computer_guess)
            ze_word = computer_guess
            correct_words.remove(computer_guess)
            print_computer_answer(ze_word, computer_word, new_window2)
            stdscr.refresh()
            if len(computer_guess) >= 3 and len(computer_guess) <= 4:
                computer_score += 1 
            elif len(computer_guess) == 5:
                computer_score += 2
            elif len(computer_guess) == 6:
                computer_score += 3
            elif len(computer_guess) == 7:
                computer_score += 5
            elif len(computer_guess) > 7 and len(computer_guess) <= 16:
                computer_score += 11
            computer_time = time.time() + 7

        if not correct_words:
            break

        choice = stdscr.getch(y, x+cursor_place)

        if choice == -1:
            continue
        elif chr(choice) == '\n':
            cursor_place = 0
            stdscr.addstr(y,x,blank_string*(len(new_choice)+1))
            user_answer = new_choice
            new_choice = ""
        elif choice == curses.KEY_BACKSPACE:
            if new_choice:
                stdscr.move(11,0)
                new_choice = new_choice[:-1]
                stdscr.addstr(11,0, blank_string*18)
                stdscr.addstr(11, 0, new_choice)
                cursor_place -= 1        
                stdscr.refresh()
                continue
        elif len(new_choice) >= 17:
            continue
        else:
            redraw = True
            new_choice+=str(chr(choice))
            cursor_place+=1
        
        if user_answer in correct_words:
            guessed_word.append(user_answer)
            ze_word = user_answer
            correct_words.remove(user_answer)
            if not correct_words:
                break
            print_player_answer(ze_word, guessed_word, new_window)
            stdscr.refresh()
            if len(user_answer) >= 3 and len(user_answer) <= 4:
                player_score += 1 
            elif len(user_answer) == 5:
                player_score += 2
            elif len(user_answer) == 6:
                player_score += 3
            elif len(user_answer) == 7:
                player_score += 5
            elif len(user_answer) > 7 and len(user_answer) <= 16:
                player_score += 11
            new_choice = ""
            user_answer = ""

        if not correct_words:
            break

        if redraw:
            redraw = False
            stdscr.refresh()

    stdscr.getch()
    curses.endwin()
    
if __name__ == "__main__":
    curses.wrapper(main)
    print("Player score: ", player_score)
    print("Computer score: ", computer_score)
    print(all_words)        