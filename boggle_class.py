#!/usr/bin/python3

import random
import sys

#Hasbro 1976-1986 edition found at http://www.bananagrammer.com/2013/10/the-boggle-cube-redesign-and-its-effect.html
class Board:
	def __init__(self):
		self._dice = {0:['A', 'A', 'C', 'I', 'O', 'T'], 1:['A', 'B', 'I', 'L', 'T', 'Y'], 2:['A', 'B', 'J', 'M', 'O', 'Q'], 
		    3:['A', 'C', 'D', 'E', 'M', 'P'], 4:['A', 'C', 'E', 'L', 'R', 'S'], 5:['A', 'D', 'E', 'N', 'V', 'Z'], 
		    6:['A', 'H', 'M', 'O', 'R', 'S'], 7:['B', 'I', 'F', 'O', 'R', 'X'], 8:['D', 'E', 'N', 'O', 'S', 'W'], 
		    9:['D', 'K', 'N', 'O', 'T', 'U'], 10:['E', 'E', 'F', 'H', 'I', 'Y'], 11:['E', 'G', 'K', 'L', 'U', 'Y'], 
		    12:['E', 'G', 'I', 'N', 'T', 'V'], 13:['E', 'H', 'I', 'N', 'P', 'S'], 14:['E', 'L', 'P', 'S', 'T', 'U'], 
		    15:['G', 'I', 'L', 'R', 'U', 'W']}
